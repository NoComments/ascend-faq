PYACL推理报错现象：


![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180344_f87b9032_8028950.png "屏幕截图.png")

 

 

报错日志：

[ERROR] GE(10377,python3):2021-07-15-17:50:25.084.323 [davinci_model.cc:3384]10377 CheckInputAndModelSize: ErrorNo: 145000(Parameter invalid.) Input size [300] can not be smaller than op size [1248] after 64-byte alignment

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180355_6f0edbe3_8028950.png "屏幕截图.png")

 

这里我们可以看出“64字节对齐后，输入大小[300]不能小于op大小[1248]”

也就说我们的输入太小，这个时候去排查一下输入数据。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180405_c1a9107b_8028950.png "屏幕截图.png")

这个地方需要改成 四字节的int32

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180433_da060075_8028950.png "屏幕截图.png")

 

再次推理 ，正确：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180439_55c82ee3_8028950.png "屏幕截图.png")
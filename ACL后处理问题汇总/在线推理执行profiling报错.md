 **问题描述：** 
在线推理，执行profiling在后处理那块有报错

![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/113153_12145103_8049142.png "3.png")

 **定位关键：** 
多次执行profiling ,发现每次到后处理脚本执行时报错，应该是环境还在执行别的python导致的冲突导致的，经过排查发现worker机触发了任务，导致环境执行到后处理处失败

 **解决办法：** 
待worker机任务跑完后，再执行用例profiling,不会再报错
# Ascend-FAQ
onnx算子功能及各参数含义等基础知识请自查onnx算子库 https://github.com/onnx/onnx/blob/master/docs/Operators.md
#### 介绍
Ascend 预处理、模型转换、后处理等相关问题汇总 **推理部署wiki** : https://gitee.com/ascend/samples/wikis  **模型训练wiki** :https://gitee.com/ascend/modelzoo/wikis


#### 软件架构
CANN 3.0

#### ACL后处理

#### 1.HwHiAiUser用户推理时每次需要输入密码，是否有办法取消该步骤？
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/101946_4fe90ce6_8049142.png "屏幕截图.png")

 **解决办法** ：在/etc/sudoers中为其添加权限
HwHiAiUser ALL=(root) NOPASSWD:ALL

### 问题描述

按照文档安装完驱动和固件、准备安装及运行用户、安装OS依赖后

```
./Ascend-Toolkit-20.0.0.RC1-arm64-linux_gcc7.3.0.run  --install
```

报错：

```
[ERROR] ERR_NO:0x0080;ERR_DES:Package is not installed on the path /usr/local/Ascend/ascend-toolkit/20.0.0.RC1/arm64-linux_gcc7.3.0, uninstall failed.
[Toolkit] [20210609-15:16:33] [ERROR] packe_name=Ascend-toolkit-1.73.5.1.b050-centos7.6.aarch64.run install failed
[Toolkit] [20210609-15:16:33] [ERROR] package install failed
[Toolkit] [20210609-15:16:33] [ERROR] process exit
```

查看详细日志：

```
vi /var/log/ascend_seclog/ascend_install.log

[toolkit] [2021-06-09 14:26:03] [INFO] InstallStart
[toolkit] [2021-06-09 14:26:03] [INFO] LogFile: /var/log/ascend_seclog/ascend_install.log
[toolkit] [2021-06-09 14:26:03] [INFO] OperationLogFile: /var/log/ascend_seclog/operation.log
[toolkit] [2021-06-09 14:26:03] [INFO] InputParams: --quiet --full --install-path=/usr/local/Ascend/ascend-toolkit/20.0.0.RC1/arm64-linux_gcc7.3.0
[toolkit] [2021-06-09 14:26:03] [INFO] base version is none.
[toolkit] [2021-06-09 14:26:03] [INFO] install targetdir=/usr/local/Ascend/ascend-toolkit/20.0.0.RC1/arm64-linux_gcc7.3.0, type=full, quiet=y.
[toolkit] [2021-06-09 14:26:03] [INFO] step into run_toolkit_install.sh ...
[toolkit] [2021-06-09 14:26:05] [INFO] install package succeed.
[toolkit] [2021-06-09 14:26:05] [INFO] copy config file succeed.
[toolkit] [2021-06-09 14:26:05] [ERROR] Install profiling failed, please check and retry!
[toolkit] [2021-06-09 14:26:05] [INFO] uninstall targetdir=/usr/local/Ascend/ascend-toolkit/20.0.0.RC1/arm64-linux_gcc7.3.0, type=full, quiet=y.
[toolkit] [2021-06-09 14:26:05] [INFO] step into run_toolkit_uninstall.sh ...
[toolkit] [2021-06-09 14:26:05] [INFO] Uninstall profiling succeed!
[toolkit] [2021-06-09 14:26:05] [INFO] Remove /usr/local/Ascend/ascend-toolkit/20.0.0.RC1/arm64-linux_gcc7.3.0 files succeed!
[toolkit] [2021-06-09 14:26:05] [ERROR] Toolkit package install failed, please retry!
[toolkit] [2021-06-09 14:26:05] [INFO] InstallEnd
```

### 初步定位：
参考：http://3ms.huawei.com/km/blogs/details/8791495  

这个版本太早了，日志收集的不全，没有打屏日志的，只能看到profiling 安装失败，原因大概率是   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/165621_3dc9d47e_8049142.png "屏幕截图.png")

### 解决办法：

```
pip3.7 install grpcio
后再次安装cann 成功
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/165655_65d8435a_8049142.png "屏幕截图.png")    

![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/165730_a34b2cb1_8049142.png "屏幕截图.png")  

再配置环境变量后大功告成。 
### Atlas 500传输文件方法


在开发者模式设置其他
vi /etc/resolv.conf
改为
nameserver 114.114.114.114
测试是否能上网ping baidu.com


#### 问题描述：直接传输文件到500小站，一直卡住  
![1](https://images.gitee.com/uploads/images/2021/0824/105117_a09d5560_8049142.png "屏幕截图.png")

#### 解决办法：使用SCP 传输到小站指定路径。（root用户，admin用户权限不够）  

可能出现permission denied    
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/105630_e979fc61_8049142.png "屏幕截图.png")  
 **1、在普通模式打开sftp**    
```sftp enable```    
 **2、需要开通root权限  并注释HostKeyAlgorithms\DenyUsers\DenyGroups**        
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/105714_9bd8da7c_8049142.png "屏幕截图.png")  
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/scp_500image.png)  
然后即可传输到tmp目录

然后使用scp 传输成功，root用户默认为develop默认密码 Huawei@SYS3  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0824/105948_355388d9_8049142.png "屏幕截图.png")  
# Ascend-FAQ

#### 介绍
Ascend 预处理、模型转换、后处理等相关问题汇总 **推理部署wiki** : https://gitee.com/ascend/samples/wikis  **模型训练wiki** :https://gitee.com/ascend/modelzoo/wikis


#### 软件架构
CANN 3.0

#### 环境安装问题

#### 1、在ctrlcpu环境执行冒烟测试的时候，发现缺很多Python第三方库，执行BERT类用例的时候，一直提示第三方pandas、_bz2库无法安装，pip3 install _bz2无法安装成功
![输入图片说明](https://images.gitee.com/uploads/images/2021/0422/104542_445e6401_8049142.png "环境安装1.png")

 **解决办法** ：
1．找到环境的Python路径进入到lib-dynload目录下，/usr/local/python3.7/lib/python3.7/lib-dynload/

2．要到_bz2.cpython-37m-x86_64-linux-gnu.so文件，将文件放到这个路径下面

3．进入到Python：Python3.7

4．import pandas


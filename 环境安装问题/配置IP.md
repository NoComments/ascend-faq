#### 配置网络

1、atlas500
```
cd etc/sysconfig/network-scripts  

BOOTPROTO=dhcp
DEVICE="eth0"
ONBOOT=yes
DEFAULTROUTE=yes
重启网络
service network restart

BOOTPROTO=""
DEVICE="eth0"
BOOTPROTO=dhcp
IPADDR="192.168.2.111"
NETMASK="255.255.255.0"
STARTMODE="auto"
ARPCHECK="no"
ONBOOT=yes
DEFAULTROUTE=yes
DNS1=114.114.114.114
```
2、ubuntu18 
```
Netplan配置文件位于目录/etc/netplan/中，进入Ubuntu 18.04系统后，执行cd /etc/netplan/
执行vim 01-netcfg.yaml编译yaml文件，添加如下内容（需要配置的网卡名为eth0）：
  ethernets:
                enp125s0f0:  
                            dhcp4: no #dhcp4关闭，使用静态IP
                            addresses: [193.168.129.231/24] #设置本机IP及掩码
                            gateway4: 193.168.129.1 #设置网关
                            nameservers:
                                     addresses: [114.114.114.114] #设置DNS
执行sudo netplan apply使配置 重启生效

network:
  version: 2
  renderer: networkd
  ethernets:
    enp125s0f3:
      dhcp4: yes
```
3、centos7
```
修改网络cd etc/sysconfig/network-scripts  
vi ifcfg-enp3s0
修改或添加(其中IP地址先ping，不通则没有人占用，则该地址可用)
BOOTPROTO=static
IPADDR=193.168.129.233
NETMASK=255.255.255.0
GATWAY=193.168.129.1
DNS1=114.114.114.114
ONBOOT=yes
修改网关vi /etc/sysconfig/network
NETWORKING=yes
HOSTNAME=centos
GATEWAY=193.168.129.1
重启网络
service network restart
```
4、临时脚本set_ip.sh
```
#!/bin/bash
ifconfig enp125s0f0 up 192.168.30.12/24
route add default gw 192.168.30.1
echo >> /etc/resolv.conf
echo 'nameserver 114.114.114.114' >> /etc/resolv.conf
```
##### 问题描述：搭建pyacl demo过程中安装live555后，import live555报错  
demo链接：https://gitee.com/ascend-fae/py-acl_demo/tree/master/pyACL_demo  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0508/141614_ae138ca0_8049142.png "屏幕截图.png")  
import live555报错，但是没有指导里说的/lib64/libssl.so.1.0.2k这个文件

##### 解决办法：找到系统中find / -name "libssl.so*"位置，强制LD_PRELOAD载入ssl库  
export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libssl.so.1.1  


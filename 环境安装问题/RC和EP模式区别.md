https://support.huaweicloud.com/instg-cli-cann330-alpha002/atlasrun_03_0004.html  
以昇腾310 AI 处理器的PCle的工作模式进行区分，如果PCle工作在主模式，可以扩展外设，则称为RC模式；如果PCle工作在从模式，则称为EP模式。具体可参考推理场景产品形态说明。
https://support.huaweicloud.com/instg-cli-cann330-alpha002/atlasrun_03_0182.html  
以昇腾 AI 处理器的PCIe的工作模式进行区分，如果PCIe工作在主模式，可以扩展外设，则称为RC模式；如果PCIe工作在从模式，则称为EP模式。

昇腾 AI 处理器的工作模式如下：
昇腾310 AI处理器有EP和RC两种模式。
昇腾710 AI处理器只有EP模式。
昇腾910 AI处理器只有EP模式。
支持RC模式的产品有：Atlas 200 AI加速模块、Atlas 200 DK 开发者套件。
产品的CPU直接运行用户指定的AI业务软件，接入网络摄像头、I2C传感器、SPI显示器等其他外挂设备作为从设备接入产品。

支持EP模式的产品
昇腾310 AI处理器：Atlas 200 AI加速模块、Atlas 300I 推理卡、Atlas 500 智能小站、Atlas 500 Pro 智能边缘服务器、Atlas 800 推理服务器。

昇腾710 AI处理器：Atlas 300I Pro 推理卡。

昇腾910 AI处理器：Atlas 800 训练服务器、Atlas 300T 训练卡。

EP模式通常由Host侧作为主端，Device侧作为从端。客户的AI业务程序运行在Host系统中，产品作为Device系统以PCIe从设备接入Host系统，Host系统通过PCIe通道与Device系统交互，将AI任务加载到Device侧的昇腾 AI 处理器中运行。

两种模式的产品及架构如图1所示。

Host和Device的概念说明如下：

Host：是指与昇腾AI处理器所在硬件设备相连接的X86服务器、ARM服务器，利用昇腾AI处理器提供的NN（Neural-Network）计算能力完成业务。
Device：是指安装了昇腾AI处理器的硬件设备，利用PCIe接口与服务器连接，为服务器提供NN计算能力。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1018/151449_018d17d9_8049142.png "屏幕截图.png")

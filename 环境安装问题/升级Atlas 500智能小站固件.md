#### 升级Atlas 500智能小站固件  
若当前固件版本低于2.2.209.020，需要先升级到2.2.209.020版本，再升级20.x版本。  
https://support.huawei.com/enterprise/zh/ascend-computing/a500-3000-pid-250702836/software/251857140?idAbsPath=fixnode01%7C23710424%7C251366513%7C22892968%7C250702836  
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/500_0301image.png)  
已登录智能边缘管理系统 Web界面。上传固件包升级。 
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/500_0301_2image.png)  
稍等重启以后 登录查看；  
下载最新版固件同理升级 https://www.hiascend.com/hardware/firmware-drivers?tag=community  

![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/500_0301_3image.png)  


相关环境配置
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/500_0301_07image.png)  
1.	IES模式执行如下命令，开启sftp传输功能。  
sftp enable  
2.	root权限开通  
进入开发者模式，修改/etc/ssh/sshd_config 文件如下：  
PermitRootLogin yes  
#DenyUser root  
#DenyGroups root  
完成后，通过systemctl restart sshd 生效。  
3.	登录超时设置  
vi /etc/ssh/sshd_config 修改文件中ClientAliveInterval 和  
ClientAliveCountMax 的值为0。  
ClientAliveInterval 0  
ClientAliveCountMax 0  
通过命令vi /etc/profile 修改超时时间：  
export TMOUT=0  
执行source /etc/profile 使环境变量生效。  
执行以下命令，重启ssh服务。  
service sshd restart  

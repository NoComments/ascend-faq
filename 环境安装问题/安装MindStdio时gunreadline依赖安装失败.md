#### 问题：

ARM服务器安装**MindStdio**时**gunreadline**依赖安装失败：

![img](file:///C:/Users/x84187058/AppData/Roaming/eSpace_Desktop/UserData/x84187058/imagefiles/originalImgfiles/A3E3A368-0149-445B-8764-44C896DD43B4.png)

#### 服务器环境：

- OS：Ubuntu18.04.1 LTS （aarch64 ）
- CANN：5.0.2alpha003
- MindStdio：3.0.1

#### 解决：

apt安装**libncurses5-dev**和 **libffi-dev**再安装gunreadline

```shell
sudo apt-get install libncurses5-dev libffi-dev
pip3 install gunreadline --user
```

#### 安装完成！




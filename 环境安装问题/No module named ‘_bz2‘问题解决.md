### ModuleNotFoundError: No module named ‘_bz2‘问题解决  

参考https://support.huawei.com/enterprise/zh/doc/EDOC1100219211/7a7e4302使用ascend-deployer 报错问题。
```
root@ascend:/usr/local/python3.7.5/lib/python3.7/lib-dynload# ascend-download --help
Traceback (most recent call last):
  File "/usr/local/python3.7.5/bin/ascend-download", line 5, in <module>
    from ascend_deployer.downloader.downloader import main
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/downloader.py", line 32, in <module>
    import os_dep_downloader
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/os_dep_downloader.py", line 22, in <module>
    from rpm_downloader import Yum
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/rpm_downloader.py", line 22, in <module>
    import bz2
  File "/usr/local/python3.7.5/lib/python3.7/bz2.py", line 19, in <module>
    from _bz2 import BZ2Compressor, BZ2Decompressor
ModuleNotFoundError: No module named '_bz2'
```
### 解决办法：   
出现这个错误的原因是我用的是python3.7，但是bz2这个库是安装到了python3.6里面，所以找不到。为了解决这个问题，需要将python3.6里面的bz库拷贝到python3.7下面。具体过程如下：  
1、找到python3.6路径下的bz库文件，即“_bz2.cpython-36m-aarch64-linux-gnu.so”。    
ls /usr/lib/python3.6/lib-dynload/    
![输入图片说明](image.png)    
2、切换到python3.7对应路径，将该文件复制到该目录下：  
```
cd /usr/local/python3.7.5/lib/python3.7/lib-dynload
sudo cp /usr/lib/python3.6/lib-dynload/_bz2.cpython-36m-aarch64-linux-gnu.so ./
```  
3、修改文件名称，将"-36m"修改为"-37m"即可：  
```
sudo mv _bz2.cpython-36m-aarch64-linux-gnu.so _bz2.cpython-37m-aarch64-linux-gnu.so
```

再次验证  
```
root@ascend:/usr/local/python3.7.5/lib/python3.7/lib-dynload# ascend-download --help                  Traceback (most recent call last):
  File "/usr/local/python3.7.5/bin/ascend-download", line 5, in <module>
    from ascend_deployer.downloader.downloader import main
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/downloader.py", line 32, in <module>
    import os_dep_downloader
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/os_dep_downloader.py", line 22, in <module>
    from rpm_downloader import Yum
  File "/usr/local/python3.7.5/lib/python3.7/site-packages/ascend_deployer/downloader/rpm_downloader.py", line 25, in <module>
    import lzma
  File "/usr/local/python3.7.5/lib/python3.7/lzma.py", line 27, in <module>
    from _lzma import *
ModuleNotFoundError: No module named '_lzma'  
``` 
同理拷贝“_lzma.cpython-36m-aarch64-linux-gnu.so”并修改即可。  
![输入图片说明](%E5%9B%BE%E7%89%87_20211210111107.png)  
至此，问题解决。
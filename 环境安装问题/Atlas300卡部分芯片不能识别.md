关键词】推理卡、Ascend 310 NPU芯片

【适用版本】TaiShan200服务器+ Atlas300 AI加速卡

【问题现象】一张Atlas 300推理卡（PCIe标卡）上面有4颗 Ascend 310 NPU芯片,推理卡部分芯片不能识别。

在物理机上执行npu-smi info命令查看    
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_npu_0809.png)  
IBMC查看riser卡型号
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_ibmc_0809.png)  
IBMC再查看Atlas300卡的型号 
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_ibmc_0809_1.png)

参考https://support.huawei.com/enterprise/zh/doc/EDOC1100114913/fc2fb2cb  
【结论】Atlas 300I 推理卡（型号：3000）时，必须选BOM编码为02312QJV的Riser卡，即BC82PRUH的riser卡。 
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_riser_0809_1.png)  
【解决办法】更换BC82PRUH型号的riser卡，或者更换Atlas300(3010)卡，Atlas300(3010)卡与BC82PRUA的riser卡是兼容的。
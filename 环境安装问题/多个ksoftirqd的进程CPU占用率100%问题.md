#### 泰山200上出现多个ksoftirqd的进程，而且CPU时间占用率100%
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image.png)  

ethtool –I enp3s0  
可以看到现在看不到网卡的具体版本信息  
升级IN200网卡驱动:
网卡驱动下载链接：
https://support.huawei.com/enterprise/zh/management-software/taishanserver-idriver-pid-251215329/software
文件名：TaiShanServer iDriver-CentOS7.6-Driver-V111(此处安装的系统为Centos7.6)

在IBMC挂载解压出来的驱动onboard_driver_CentOS7.6.iso

命令行执行  mount /dev/sr0 /mnt

cd /mnt
安装hinicadm管理工具: 
rpm -ivh NIC-IN200-CentOS7.6-hinicadm-3.7.0.8-aarch64.rpm 
安装驱动程序: 
rpm -Uvh NIC-IN200-CentOS7.6-hinic-3.7.0.8-aarch64.rpm 

重新加载驱动： 
lsmod |grep hinic 
rmmod hinic  卸载旧驱动(会断网，需到IBMC控制台去执行下一步)  
lsmod |grep hinic 可以看到没有驱动模块了 
modprobe hinic  出现了新的驱动模块，而且大小和之前的不一样的  


升级IN200网卡后： 
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image1.png) 
可以看到网卡驱动版本信息且查看top命令没有了之前的ksoftirqd的进程占用100%的情况 
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image2.png) 

### Ubuntu配置临时IP

#### IP

```shell
ifconfig enp125s0f0 up 190.14.0.11/18
# 或者
ifconfig enp125s0f0 190.14.0.11 netmask 255.255.192.0
```

#### gateway

```shell
route add default gw 190.14.0.1
```

#### dns

```shell
# 修改配置文件
vim /etc/resolv.conf
nameserver 114.114.114.114
```

### shell脚本修改

```shell
###set-net.sh
#!/bin/bash
ifconfig enp125s0f0 up 190.16.0.15/18
route add default gw 190.16.0.1
echo >> /etc/resolv.conf
echo 'nameserver 114.114.114.114' >> /etc/resolv.conf
```

##### 设置启动项

```shell
touch /etc/init.d/net.sh
vim set-net.sh

#!/bin/bash
ifconfig enp125s0f0 up 190.16.0.15/18
route add default gw 190.16.0.1
echo >> /etc/resolv.conf
echo 'nameserver 114.114.114.114' >> /etc/resolv.conf
:wq

chomd 777 net.sh

update-rc.d net.sh defaults 90
```

### 静态IP配置参考   
Atlas200DK
```
cd /etc/netplan/

network:
  version: 2
#  renderer: NetworkManager
  renderer: networkd
  ethernets:
    eth0:
      dhcp4: no
      addresses: [192.168.8.2/24]
      gateway4: 192.168.8.1
      nameservers:
            addresses: [114.114.114.114]

    usb0:
      dhcp4: no
      addresses: [192.168.1.2/24]
      gateway4: 192.168.8.1
```
Atlas500
```
 cd etc/sysconfig/network-scripts  

BOOTPROTO=dhcp
DEVICE="eth0"
ONBOOT=yes
DEFAULTROUTE=yes
重启网络
service network restart

BOOTPROTO=""
DEVICE="eth0"
BOOTPROTO=static
IPADDR="192.168.2.111"
NETMASK="255.255.255.0"
STARTMODE="auto"
ARPCHECK="no"
ONBOOT=yes
DEFAULTROUTE=yes
DNS1=114.114.114.114 
```
Atlas800-ubuntu
```
Netplan配置文件位于目录/etc/netplan/中，进入Ubuntu 18.04系统后，执行cd /etc/netplan/
执行vim 01-netcfg.yaml编译yaml文件，添加如下内容（需要配置的网卡名为eth0）：
  ethernets:
                enp125s0f0:  
                            dhcp4: no #dhcp4关闭，使用静态IP
                            addresses: [190.14.0.14/18] #设置本机IP及掩码
                            gateway4: 190.14.0.1 #设置网关
                            nameservers:
                                     addresses: [114.114.114.114] #设置DNS
执行sudo netplan apply使配置 重启生效
```
Atlas800-centos
```
修改网络cd etc/sysconfig/network-scripts  
vi ifcfg-enp3s0
修改或添加(其中IP地址先ping，不通则没有人占用，则该地址可用)
BOOTPROTO=static
IPADDR=193.168.129.233
NETMASK=255.255.255.0
GATWAY=193.168.129.1
DNS1=114.114.114.114
ONBOOT=yes
修改网关vi /etc/sysconfig/network
NETWORKING=yes
HOSTNAME=centos
GATEWAY=193.168.129.1
重启网络
service network restart
```
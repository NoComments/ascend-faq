##### modelzoo链接：
https://www.hiascend.com/zh/software/modelzoo/detail/C/210261e64adc42d2b3d84c447844e4c7  

##### 1、dockerfile问题： 按照操作指导修改Dockerfile内容为自己的Docker环境，修改后报错：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/175529_5198ba63_8049142.png "屏幕截图.png")  
##### 解决办法： 
验证发现Dockerfile有问题，删除红框部分即可完成创建。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/175608_508d9f00_8049142.png "屏幕截图.png")  

##### 2、dockerfile问题：安装docker容器内环境apt-get install libopencv-dev报错： 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/175710_f904ac8a_8049142.png "屏幕截图.png")   
##### 解决办法： 
Docker内系统换源。  

```
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak  
sudo sed -i 's/archive.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list  
sudo apt update
```
##### 3、driver安装：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/175906_3259ea2e_8049142.png "屏幕截图.png")  
##### 解决办法：
安装残余没有删除干净，输入find / -name *ascend*，将搜索到的内容删除掉即可
##### 安装完成无法识别driver版本
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/180004_63620382_8049142.png "屏幕截图.png")  
##### 解决办法：
关机重启解决  

##### 4、使用HwHiAiUser用户进行离线模型转换报错  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/180123_1a336782_8049142.png "屏幕截图.png")  
##### 解决办法：
模型所在路径权限不够，使用chmod 修改权限即可  
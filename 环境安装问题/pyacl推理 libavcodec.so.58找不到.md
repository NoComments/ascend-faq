
#### 2. pyacl推理 libavcodec.so.58:cannot open shared object file: No such file or directory
 **详情描述** ：运行https://gitee.com/ascend/samples/tree/master/python/level2_simple_inference/1_classification/vgg16_cat_dog_picture验证环境报错
![输入图片说明](https://images.gitee.com/uploads/images/2021/0413/114108_266c111c_8049142.png "屏幕截图.png")

 **解决办法** ：编译安装ffmpeg解决，Python样例中的atlas_util库会调用ffmpeg的so文件
参考https://gitee.com/ascend/samples/blob/master/python/environment/python_ENV/README_300_CN.md

```
1.  安装pip3  
    sudo apt-get install python3-pip     
2.  安装pillow 的依赖    
    sudo apt-get install libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
3.  安装python库  
     python3.6 -m pip install --upgrade pip --user -i https://mirrors.huaweicloud.com/repository/pypi/simple   
     python3.6 -m pip install Cython numpy pillow tornado==5.1.0 protobuf --user -i https://mirrors.huaweicloud.com/repository/pypi/simple  
4.  安装python3-opencv  
    sudo apt-get install python3-opencv
5.  安装ffmpeg  
    Python样例中的atlas_util库会调用ffmpeg的so文件。 
    创建文件夹，用于存放编译后的文件  
    mkdir -p /home/HwHiAiUser/ascend_ddk/x86
    下载ffmpeg  
    cd $HOME  
    wget http://www.ffmpeg.org/releases/ffmpeg-4.1.3.tar.gz  
    tar -zxvf ffmpeg-4.1.3.tar.gz
    cd ffmpeg-4.1.3
    安装ffmpeg   
    ./configure --enable-shared --enable-pic --enable-static --disable-x86asm --prefix=/home/HwHiAiUser/ascend_ddk/x86
    make -j8     
    make install
    将ffmpeg添加到系统环境变量中，使得其他程序能够找到ffmpeg环境  
    su root  
    vim /etc/ld.so.conf.d/ffmpeg.conf  
    在末尾添加一行   
    /home/HwHiAiUser/ascend_ddk/x86/lib  
    使配置生效    
    ldconfig  

    配置profile系统文件    
    vim /etc/profile    
    在末尾添加一行  
    export PATH=$PATH:/home/HwHiAiUser/ascend_ddk/x86/bin    
    使配置文件生效    
    source /etc/profile    
    使opencv能找到ffmpeg   
    cp /home/HwHiAiUser/ascend_ddk/x86/lib/pkgconfig/\* /usr/share/pkgconfig    
    退出root用户   
    exit
```

##### 问题描述： 
caffe单通道输入的模型转成 om 后，调用 aclmdlGetInputDims 查看的模型输入维度显示是3通道的  
转换命令：
`atc --model=castDenseNet_debug.prototxt --weight=castDenseNet_debug.caffemodel --framework=0 --output=castDenseNet_debug --soc_version=Ascend310 --insert_op_conf=aipp.cfg`  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/152302_e77a5f35_8049142.png "屏幕截图.png")  
转换om后：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/152324_6657eb35_8049142.png "屏幕截图.png")  
##### 关键定位：
aipp 配置的rgb转灰度会改变通道数

##### 解决办法：
aipp改成YUV400_U8转GRAY格式,这个YUV400为单通道。  
aipp_op {  
    aipp_mode: static  
    input_format : YUV400_U8  
    csc_switch : false  
    src_image_size_w : 112  
    src_image_size_h : 112  
    min_chn_0 : 127.5  
    var_reci_chn_0 : 0.0078125  
}
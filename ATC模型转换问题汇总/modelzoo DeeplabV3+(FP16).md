#### 2.modelzoo ATC DeeplabV3+ (FP16)
https://ascend.huawei.com/zh/#/software/modelzoo/detail/1/a1f011380b8d4fc8a5cf0ca73347483c
 **详细描述** ：modelzoo中快速上手转换命令不全，导致转换推理结果为全0，和官方推理结果差异较大。

官方提供转换命令如下：
```
atc --model=./deeplabv3_tf_1.pb --framework=3 --input_format="NHWC" --input_shape=ImageTensor:1,375,500,3 --output=DeepLabV3_1 --soc_version=Ascend310 --out_nodes=SemanticPredictions:0
```
 **解决办法** ：模型转换命令，输出节点再加一个f32参数，不加该指令默认输出为INT64

```
--output_type=SemanticPredictions:0:FP32
```

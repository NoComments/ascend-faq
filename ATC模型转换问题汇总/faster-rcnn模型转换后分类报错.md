#### faster-rcnn使用benchmark推理，分类错误
issue原链接：https://gitee.com/ascend/samples/issues/I3CHAF#note_5047555  
##### 问题描述：
faster rcnn模型使用aipp转om 用benchmark来做推理，置信度和坐标正确，但分类结果不正确   
##### 初步定位：
opencv预处理 和AIPP配置冲突，需重新配置；  
 **原因：**包含AIPP且输入格式为RGB888_U8的om模型，只能接受int8格式的输入。如果在前处理用opencv进行归一化，得到的是0.几的float值的图片信息，在模型推理时被转换成了int8形式，数据的值都会丢失，导致推理不出结果。我在自己这边复现这个场景，并且把归一化放在AIPP里面做，模型输出可以得到结果。  
##### 解决办法：  
1、修改AIPP内容，把归一化放到AIPP里面做  
aipp_op {  
aipp_mode : static  
related_input_rank : 0  
input_format : RGB888_U8  
src_image_size_w : 1216  
src_image_size_h : 1216  
crop : false  
csc_switch : false  
rbuv_swap_switch : true  
mean_chn_0 : 123  
mean_chn_1 : 116  
mean_chn_2 : 103  
var_reci_chn_0 : 0.01712475383  
var_reci_chn_1 : 0.0175070028  
var_reci_chn_2 : 0.017429193  
}  
2、把代码里面归一化的功能注释掉  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/164507_d69f2a0e_8049142.png "屏幕截图.png")  
得到OM推理结果正常  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0517/164624_0ea9afdd_8049142.png "屏幕截图.png")


# Ascend-FAQ
 **onnx算子功能及各参数含义等基础知识请自查onnx算子库** 

https://github.com/onnx/onnx/blob/master/docs/Operators.md

#### 介绍
Ascend 预处理、模型转换、后处理等相关问题汇总 

**推理部署wiki** : https://gitee.com/ascend/samples/wikis  

**模型训练wiki** :https://gitee.com/ascend/modelzoo/wikis

#### 软件架构
CANN 3.0

## 目录结构与说明

**./**   
├── [ACL前处理问题汇总](./ACL前处理问题汇总)：**ACL前处理问题**    
│   ├── [aclmdlGetDatasetBuffer.md](./ACL前处理问题汇总/aclmdlGetDatasetBuffer.md)  
│   ├── [精度差异排查思路.md](./ACL前处理问题汇总/精度差异排查思路.md)    
│   └── [PYACL推理报错现象.md](./ACL前处理问题汇总/PYACL推理报错现象.md)   
├── [ACL后处理问题汇总](./ACL后处理问题汇总)：**ACL后处理问题**  
│   ├── [取消HwHiAiUser用户推理时输入密码.md](./ACL后处理问题汇总/取消HwHiAiUser用户推理时输入密码.md)  
│   ├── [在线推理执行profiling报错.md](./ACL后处理问题汇总/在线推理执行profiling报错.md)  
│   ├── [推理内存溢出.md](./ACL后处理问题汇总/推理内存溢出.md)  
├── [ATC模型转换问题汇总](./ATC模型转换问题汇总)：**ATC模型转换问题**  
│   ├── [InitializeInner Failed to open plugin so.md](./ATC模型转换问题汇总/InitializeInner%20Failed%20to%20open%20plugin%20so.md)  
│   ├── [merge算子不支持动态shape v1转v2.md](./ATC模型转换问题汇总/merge算子不支持动态shape%20v1转v2.md)   
│   ├── [单通道caffe模型转换om后为3通道.md](./ATC模型转换问题汇总/单通道caffe模型转换om后为3通道.md)     
│   ├── [faster-rcnn模型转换后分类报错.md](./ATC模型转换问题汇总/faster-rcnn模型转换后分类报错.md)         
│   └── [modelzoo DeeplabV3+(FP16).md](./ATC模型转换问题汇总/modelzoo%20DeeplabV3+(FP16).md)  
├── [modelarts环境](./modelarts环境)： **modelarts环境**  
│   ├── [SWR容器镜像服务上传镜像.md](./modelarts环境/SWR容器镜像服务上传镜像.md)         
├── [环境安装问题](./环境安装问题)： **环境安装问题**   
│   ├── [pyacl推理 libavcodec.so.58找不到.md](./环境安装问题/pyacl推理%20libavcodec.so.58找不到.md)     
│   ├── [三方pandas、_bz2库无法安装，pip3 install _bz2无法安装成功.md](./环境安装问题/三方pandas、_bz2库无法安装，pip3%20install%20_bz2无法安装成功.md)  
│   ├── [modelzoo_yolov3训练环境问题.md](./环境安装问题/modelzoo_yolov3训练环境问题.md)   
│   ├── [import live555报错SSL_CTX_free.md](./环境安装问题/import%20live555报错SSL_CTX_free.md)  
│   ├── [驱动安装失败.md](./环境安装问题/驱动安装失败.md)     
│   ├── [cann安装失败ERR_NO0x0080.md](./环境安装问题/cann安装失败ERR_NO0x0080.md)   
│   ├── [安装操作系统时黑屏.md](./环境安装问题/安装操作系统时黑屏.md)  
│   ├── [安装MindStdio时gunreadline依赖安装失败.md](./环境安装问题/安装MindStdio时gunreadline依赖安装失败.md)     
│   ├── [atlas500传输文件.md](./环境安装问题/atlas500传输文件.md)     
│   ├── [RC和EP模式区别.md](./环境安装问题/RC和EP模式区别.md)     
│   ├── [200DK极简环境搭建.md](./环境安装问题/200DK极简环境搭建.md)    
│   ├── [No module named ‘_bz2’问题解决](./环境安装问题/No%20module%20named%20‘_bz2‘问题解决.md)    
│   ├── [ubuntu配置临时ip](./环境安装问题/ubuntu配置临时ip.md)   
│   ├── [多个ksoftirqd的进程CPU占用率100%问题.md](./环境安装问题/多个ksoftirqd的进程CPU占用率100%25问题.md )  
│   ├── [升级Atlas500智能小站固件.md ](./环境安装问题/升级Atlas%20500智能小站固件.md )  
├── [推理优化问题汇总](./推理优化问题汇总)： **推理优化问题**  
│   ├── [推理结果精度优化问题.md](./推理优化问题汇总/推理结果精度优化问题.md)   
│   ├── [TextCNN端到端性能优化.md](./推理优化问题汇总/TextCNN端到端优化思路/TextCNN端到端性能优化.md)   
│   ├── [TF在线推理优化.md](./推理优化问题汇总/TF在线推理优化.md)   
│   ├── [多路解码卡死问题.md](./推理优化问题汇总/多路解码卡死问题.md)  
│   ├── [docker日志获取问题.md](./推理优化问题汇总/docker日志获取问题.md)   
│   ├── [一键式全流程对比报错.md](./推理优化问题汇总/一键式全流程对比报错.md)    
├── [干货分享](./干货分享)： **干货分享**  
│   ├── [yolov5应用案例详解.md](./干货分享/MindX%20SDK+Pytorch%20yolov5应用案例详解.md)  
│   ├── [200DK--socket通信.md](./干货分享/200DK--socket通信.md)   
│   ├── [Profiler性能调优.md](./干货分享/Profiler性能调优.md)    
│   ├── [Ascend推理快速入门及ACL跑通.md](./干货分享/Ascend推理快速入门及ACL跑通.md)   



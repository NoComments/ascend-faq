#### bert(tf)在线推理优化问题
参考：https://gitee.com/ascend/samples/issues/I3QDJ7?from=project-issue  
##### 问题描述：
在线推理参考文档：https://support.huaweicloud.com/tensorflow-cann330alpha2training/atlasoiug_26_0001.html  
使用在线推理方式，测试1000条数据与CPU结果比对：  
在NPU上对模型进行fp32的编译，有98条结果不同（其中68条在小数点后2位后3位不一致）；  
在GPU上，有80条结果不同（都是小数点后4位后5位不一致）；   
##### 解决办法：   
推理过程添加参数提升精度  
PRECISION_MODE=allow_fp32_to_fp16  
OP_SELECT_IMPLMODE=high_precision  
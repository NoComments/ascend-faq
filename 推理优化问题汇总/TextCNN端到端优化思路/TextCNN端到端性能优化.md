### 模型端到端性能优化思路
#### 1、软硬件配置  
硬件配置：Atlas800 (Model 3000)+1*Atlas300  (Model 3000)  
软件配置：  
OS：Kylin Linux Advanced Server V10  
软件版本：1.75.T15.0.B150  
#### 2、测试内容  
输入： 总共50000个样本存放在一个文本文件中，每个样本一行为300个整形数据。  
输出：端到端时延，推理结果  
衡量指标：推理结果正确，端到端时延  
#### 3、测试目标  
Atlas300 (Model 3000)端到端时延接近纯推理时延。  
#### 4、优化方法  
##### 4.1模型算子融合  
如下图为TextCNN原始模型，由于Squeeze算子存在，无法构造融合算子Conv2D+BiasAdd+Relu。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/174123_d4a15e81_8049142.png "屏幕截图.png")    
修改后的模型如下图，移动Squzee算子到concatV2算子后，构造融合算子Conv2D+BiasAdd+Relu，可以提升性能，同时三个max算子的reduce轴改为2，concatV2算子的axis改为2。 
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/174330_dcdc7548_8049142.png "屏幕截图.png")  
 **注：** 原模型的cast算子作用为将fp32数据转为int32，由于Ascend310不支持直接转换，实际为fp32转fp16再转int32，fp32转fp16过程存在精度损失，影响精度。  
 **解决方案：** 去除了cast算子，输入数据类型改为int32。  

模型修改方法：将pb文件转为pbtxt文件，手动编辑文件，最后将pbtxt文件转为pb文件。[convert_txt_pb.py](./convert_txt_pb.py) 
##### 4.2异步推理
执行模型推理同步接口，直到推理结果输出才返回。  
aclError aclmdlExecute(uint32_t modelId, const aclmdlDataset *input, aclmdlDataset *output)  
执行模型推理异步接口，直接返回，不用等待推理完成。  
aclError aclmdlExecuteAsync(uint32_t modelId, const aclmdlDataset *input, aclmdlDataset *output, aclrtStream stream)  
##### 4.3数据分片方式
Atlas300 3000总共4个芯片，每个芯片处理1/4的数据。输入为一个50000行的文本文件。  
方案一：shell脚本实现，将输入文件均分成4份文件，4个芯片分别读取进行推理。  
方案二：python脚本实现,将输入文件均分成4份文件，4个芯片分别读取进行推理。  
方案三：C++实现，不对文件进行切分，4个芯片同时读同一份文件，从文件不同行号开始读。耗时最少。  
##### 4.4 软件流水
通过打点计时分析时各个模块的时延，下面为软件执行流程，以2次推理流程举例：  
方案一：同步推理，串行执行，时间线性累加  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/174956_e0c70d53_8049142.png "屏幕截图.png")  
方案二：异步推理，所有数据一次准备好，数据读取时间线性累加  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/175051_e2771c7b_8049142.png "屏幕截图.png")    
方案三：异步推理，每次只读取整batch数据，后处理单独开启一个线程，由回调函数实现。耗时最短。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/175123_4f345ec6_8049142.png "屏幕截图.png")  
##### 4.5 batchsize选取
尝试不同batchsize下的推理测试，找到时延最小的batchsize。实测bs大点较好，bs256差不多性能到顶了。  
实现过程：  
Shell脚本实现，拉起4个进程，分别跑在4个device。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/175228_4d89f5a7_8049142.png "屏幕截图.png")  
执行测试，获取端到端时延  
time ./test.sh 4 256   指定4个device，batchsize为256  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0507/175319_f4c8d9e4_8049142.png "屏幕截图.png")  


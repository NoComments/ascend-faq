#### docker安装参考  
https://docs.docker.com/engine/install/ubuntu/

    sudo docker run hello-world  测试运行
    docker images  查看镜像
    docker pull ubuntu:18.04  拉取镜像
    docker run -it --name="ubuntu18.04" ubuntu:18.04 创建容器
    docker attach ubuntu18.04 进入后台运行中的容器
    修改容器中的源
    cp -a /etc/apt/sources.list /etc/apt/sources.list.bak
    sed -i "s@http://.*archive.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list
    sed -i "s@http://.*security.ubuntu.com@http://repo.huaweicloud.com@g" /etc/apt/sources.list 
    apt-get update
    
    安装依赖
    apt-get install -y vim gcc g++ make cmake unzip zlib1g-dev libbz2-dev libsqlite3-dev libssl-dev libxslt1-dev libffi-dev
    
    docker外执行：docker cp Python-3.7.5.tgz ubuntu18.04:/opt
    docker里执行：
    cd /opt
    tar –xzvf  Python-3.7.5.tgz
    
    配置，编译，安装
    ./configure --prefix=/usr/local/python3.7.5 --enable-shared
    make
    make install
    
    将编译后的文件复制到/usr/lib
    cp /usr/local/python3.7.5/lib/libpython3.7m.so.1.0 /usr/lib
    
    设置软连接
    ln -s /usr/local/python3.7.5/bin/python3 /usr/bin/python3.7.5
    ln -s /usr/local/python3.7.5/bin/pip3 /usr/bin/pip3.7.5
    ln -s /usr/local/python3.7.5/bin/python3 /usr/bin/python3.7
    ln -s /usr/local/python3.7.5/bin/pip3 /usr/bin/pip3.7

    查看是否安装成功
    python3.7 --version
    pip3.7 --version
    
 ![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/docker_0804_1.png)   
    
2、制作Docker镜像
  
    在docker外执行
    docker commit -m "python3.7.5 installed in ubuntu18.04" -a "xw" ubuntu18.04 python3.7.5-ubuntu18.04:1.0.0
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-docker-0804-2.png)
        
    
    制作好的镜像打包成tar包  
    docker save -o /opt/python3.7.5-ubuntu18.04.tar python3.7.5-ubuntu18.04:1.0.0  
    docker save -o tar包的名字 镜像名  
    上传至其他服务器，测试能否使用  
    scp /opt/python3.7.5-ubuntu18.04.tar root@2.0.0.241:/home/84172999  
    scp  tar包所在路径  用户名@IP地址：要上传的路径
    
3、使用镜像

    docker load < /home/python3.7.5-ubuntu18.04.tar
    docker load < tar 包所在路径

 ### **实例：升级镜像中cann包和torch** 
```
    docker load < xxx.tar
    docker run -it --ipc=host -v /root/cann/5.1.RC2/:/root/cann/5.1.RC2 swr.cn-central-231.xckpjs.com/notebook/pytorch:1.5V2 /bin/bash
1、在容器中升级cann包
    ./Ascend-cann-toolkit_5.1.RC2_linux-aarch64.run --update
2、安装whl包
    pip3 install apex-0.1+ascend.20220712-cp37-cp37m-linux_aarch64.whl
    pip3 install torch-1.5.0+ascend.post6.20220712-cp37-cp37m-linux_aarch64.whl
```
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_1026_1.png)

    安装torch包失败，解决办法：修改EulerOS源，安装依赖，再次安装成功
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image_1026_2.png)
    yum install python3-devel
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-1026-3.png)
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-1026-4.png)
3、安装成功截图
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-1026-5.png)

4、制作镜像
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-1026-6.png)
5、保存镜像
![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/image-1026-7.png)
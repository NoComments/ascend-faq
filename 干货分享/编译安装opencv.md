#### 安装opencv
参考https://blog.csdn.net/haoqimao_hard/article/details/82049565
```
root用户
git clone -b 4.3.0 https://gitee.com/mirrors/opencv.git
git clone -b 4.3.0 https://gitee.com/mirrors/opencv_contrib.git
cd opencv
mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules/ ..

cmake -D BUILD_SHARED_LIBS=ON  -D BUILD_opencv_python3=YES -D BUILD_TESTS=OFF -D CMAKE_BUILD_TYPE=RELEASE -D  CMAKE_INSTALL_PREFIX=/home/84172999/opencv -D WITH_LIBV4L=ON -D OPENCV_EXTRA_MODULES=../../opencv_contrib/modules -D PYTHON3_LIBRARIES=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu/libpython3.7m.so  -D PYTHON3_NUMPY_INCLUDE_DIRS=/usr/local/lib/python3.7/site-packages/numpy/core/include/ -D OPENCV_SKIP_PYTHON_LOADER=ON ..

–j8
make install

sudo echo "/usr/local/lib " >> /etc/ld.so.conf.d/opencv.conf 
sudo ldconfig  

sudo gedit /etc/bash.bashrc
PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig  
export PKG_CONFIG_PATH
source /etc/bash.bashrc  
sudo updatedb

检查版本pkg-config --modversion opencv
```
##### [技术干货-socket通信] 

 **1、200DK起一个server.py** 

```
# -*- coding: UTF-8 -*-
import socket #导入socket

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #创建socket
server.bind(('190.7.0.18', 8001)) #绑定200DK的IP/端口
server.listen(5) #可以挂起的最大连接数
print('starting....')
while True:
    conn, addr = server.accept() #等待链接,会阻塞，直到client发起链接 conn打开一个新的对象
    print(conn)
    print('client addr', addr)
    client_msg = conn.recv(1024)
    print('receive client message: %s' % client_msg)
    if client_msg == 'bye':break  #收到bye时退出
    conn.send(client_msg.upper())
    conn.close()
server.close()
```
启动server.py 后端服务     
![输入图片说明](https://images.gitee.com/uploads/images/2021/0927/110035_cf24d6e4_8049142.png "屏幕截图.png")  

 **2、PC端client.py发送信息**   

```
# -*- coding: UTF-8 -*-
import socket

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #创建socket实例
client.connect(('190.7.0.18', 8001)) #连接server端IP地址/端口按你自己实际情况来
msg = "Hello Server!"
#msg = "bye"
client.send(msg.encode('utf-8')) #发送消息给server端
back_msg = client.recv(1024)
print(back_msg)
client.close() #关闭socket
```
启动client.py   
![输入图片说明](https://images.gitee.com/uploads/images/2021/0927/110314_ee9d3630_8049142.png "屏幕截图.png")  

server端即可监听收到消息，socket通信测试成功。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0927/110424_6b8e3905_8049142.png "屏幕截图.png")



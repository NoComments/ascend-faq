
## obsutil安装完成后报错-bash: /usr/bin/obsutil: No such file or directory
```
root@ubuntu:~# tar -xvf obsutil_linux_arm64.tar.gz 
obsutil_linux_arm64_5.3.4/
obsutil_linux_arm64_5.3.4/setup.sh
obsutil_linux_arm64_5.3.4/obsutil
root@ubuntu:~# cd obsutil_linux_arm64_5.3.4/
root@ubuntu:~/obsutil_linux_arm64_5.3.4# ls
obsutil  setup.sh
root@ubuntu:~/obsutil_linux_arm64_5.3.4# bash setup.sh ./
no expected param that the path of obsutil!
root@ubuntu:~/obsutil_linux_arm64_5.3.4# bash setup.sh ./obsutil 
give /obsutil all permissions.
copy /root/obsutil_linux_arm64_5.3.4/obsutil to /obsutil and give it all permissions.
setup.sh: line 55: /usr/bin/obsutil: No such file or directory
root@ubuntu:~/obsutil_linux_arm64_5.3.4# 
root@ubuntu:~/obsutil_linux_arm64_5.3.4# 
root@ubuntu:~/obsutil_linux_arm64_5.3.4# 
root@ubuntu:~/obsutil_linux_arm64_5.3.4# ls /usr/bin/obsutil
/usr/bin/obsutil
root@ubuntu:~/obsutil_linux_arm64_5.3.4# uname -a
Linux ubuntu 4.15.0-29-generic #31-Ubuntu SMP Tue Jul 17 15:41:03 UTC 2018 aarch64 aarch64 aarch64 GNU/Linux
root@ubuntu:~/obsutil_linux_arm64_5.3.4# obsutil config
-bash: /usr/bin/obsutil: No such file or directory
```

#### 解决办法
```
mkdir /lib64
ln -s /lib/ld-linux-aarch64.so.1 /lib64/ld-linux-aarch64.so.1
```

#### obsutil常见操作

```
登录成都智算OBS服务
./obsutil config -i=ak -k=sk -e=https://obs.cn-southwest-228.cdzs.cn
登录成功即可访问，传输，同步等操作
./obsutil ls -s
./obsutil_linux_arm64_5.3.4/obsutil cp tools/ obs://bear/test/  -r -f
./obsutil_linux_arm64_5.3.4/obsutil sync tools/ obs://bear/test/
```
![输入图片说明](images/cdzs_0426_1.png)
# 成都智算ModelArts 操作指南

## 一、前期准备

### 1. 登录进入计算中心平台

* [计算中心登录界面](https://www.cdzs.cn/)
* 请联系华为项目接口人获取计算中心平台账号（同一个项目只申请一个账号）
* 计算中心控制台界面如下：
![输入图片说明](images/zszx_0424.png)

### 2. Modelarts使用概览

进入Modelarts控制台后，主要关注下图中开发环境、训练管理、专属资源池和全局配置四个板块内容和右上角帮助文档，后面均会有详细的介绍说明！
![输入图片说明](images/cdzs_0424_1.png)

## 二、OBS存储软件使用

**【说明】： 通常我们的数据集、工程代码、训练输出文件等都会存储在OBS中，因此需要了解OBS的使用方法！**

* **OBS下载及登录**

  由于obs桶创建后，在计算中界面上直接上传文件存在大小限制，无法上传大文件，因此**推荐使用OBS Browser Plus 软件进行桶的创建和数据上传**（点击对象存储控制台，进入对象存储服务下载）
![输入图片说明](images/cdzs_0424_2.png)

  * 登录账号：

  使用客户端工具前，请先获取访问密钥(AK和SK)，在智算中心首页右上角获取
![输入图片说明](images/zszx_0424.png)
下载成功后，按照提示进行安装，成都智算服务器地址为119.6.186.84:443。
![输入图片说明](images/cdzs_0424_4.png)

* **OBS桶内创建相关文件夹 **

  由于后续在ModelArts平台上使用OBS存储方式进行模型开发时，需要提前在OBS中**存储数据集、工程代码文件、训练/评估/推理输出结果**等文件，**因此建议提前在自己的obs桶中建立相关文件夹。**

* [OBS使用文档](https://support.huaweicloud.com/obs/)
* [服务器使用obsutils指南](https://support.huaweicloud.com/utiltg-obs/obs_11_0003.html)  
  obsutils安装使用问题[跳转链接](https://gitee.com/Chengdu_Ascend/ascend-faq/blob/master/modelarts%E7%8E%AF%E5%A2%83/obsutil%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90%E5%90%8E%E6%8A%A5%E9%94%99.md)
  
* **OBS相关语法框架moxing**

  这里需要开发者提前了解华为提供的MoXing语法，学习链接：[MoXing开发指南](https://support.huaweicloud.com/moxing-devg-modelarts/modelarts_11_0001.html)

## 三、SWR容器镜像服务
* SWR容器镜像服务上传镜像[跳转链接](https://gitee.com/Chengdu_Ascend/ascend-faq/blob/master/modelarts%E7%8E%AF%E5%A2%83/SWR%E5%AE%B9%E5%99%A8%E9%95%9C%E5%83%8F%E6%9C%8D%E5%8A%A1%E4%B8%8A%E4%BC%A0%E9%95%9C%E5%83%8F.md)

## 四、ModelArts 之开发调试
### 1. NoteBook 实例创建

**Step1**：点击Modelarts控制台中开发环境下的NoteBook，进入NoteBook创建界面
![输入图片说明](images/cdzs_0424_5.png)
**Step2**: 点击创建后，我们将看到以下界面：填入自定义名称、推荐开启自动停止功能（节省资费）、选择公共镜像、选择公共资源池、根据镜像类型及模型任务选择Ascend\GPU\CPU类型及规格。

**注意**： 这里创建的Notebook通常可以用于实时修改代码和单机调试，但是可能机器环境和实际云上环境不同，所以可作为代码实时修改的工具之一，方便快速在云上调试。
![输入图片说明](images/cdzs_0424_6.png)

**Step3**： 完成NoteBook配置后，按照提示“下一步”并“提交”，完成NoteBook实例的创建，即可上传代码单机调试。

![输入图片说明](images/cdzs_0424_7.png) 
* 如果需要ssh远程开发，可以创建密钥对，然后添加本机IP白名单，存在多个需,隔开（本机IP可以百度查询），创建完成后即可ssh远程开发。
![输入图片说明](images/notebook_0517_3.png)
![输入图片说明](images/notebook_0517_4.png)

* 或者将本地ssh的公钥导入到modelarts

![输入图片说明](images/notebook_0517_1.png)  
![输入图片说明](images/notebook_0517_2.png)  

### 2.ModelArts 训练管理

我们准备好obs的数据集和工程代码之后，就可以使用“训练管理”服务来进行“训练作业”的创建，这里我们使用的是专属资源池来进行任务的训练，因此需选择自定义算法和自定义镜像，详细流程如下。

#### 1. 训练作业创建

* **训练作业配置1（obs数据集）** 
[使用mindspore-yolov5为案例](https://gitee.com/mindspore/models/tree/master/official/cv/yolov5)    
**Step1**： 创建算法
![输入图片说明](images/cdzs_0424_8.png)
**Step2**： 创建训练作业，提交后可查看训练日志
![输入图片说明](images/cdzs_0424_9.png)
![输入图片说明](images/cdzs_0424_10.png)
![输入图片说明](images/cdzs_0424_11.png)

#### 常规训练作业_[demo演示](https://gitee.com/Chengdu_Ascend/ascend-faq/tree/master/modelarts%E7%8E%AF%E5%A2%83)
#### 2. 训练作业配置说明

**【注意】：** 需要开发者了解以下几点：

* **1、由于训练服务开启时，ModelArts会自动申请一块新的内存环境，将指定的obs代码目录下所有文件copy到/home/ma-user/modelarts/user-job-dir/目录下，即会在内部自动执行以下命令：**

  ```python
  import moxing as mox
  src_url = "s3://your_bucket_name/src/mindvision" # 和"obs://..."等价
  cur_job_path = "/home/ma-user/modelarts/user-job-dir/mindvision"
  mox.file.copy_parallel(src_url, cur_job_path)
  ```

* **2、启动训练脚本train.py参数配置需要和相应的config文件中配置路径一致，即在上面configs文件中针对yolov5的配置文件路径均要做相应更改，比如：**

  ```
  /default_config.yaml文件中:
  
  ann_file："/home/ma-user/work/dataset/annotations/instances_train2017.json"  更改为：
  
  ann_file："/home/ma-user/modelarts/user-job-dir/dataset/annotations/instances_train2017.json"
  
  其他路径同理，全部更改到实际训练时的工程路径即可!!!
  ```

* **3、另外，即使在ModelArts上创建训练任务时指定了obs数据集的路径，但MindSpore系列的源码目前并不支持直接从obs桶中读取数据集，而是采用MoXing框架将obs桶内数据集copy到训练的工程目录下，即会执行以下代码：**

  ```python
  # if the code runs in ModelArts, copy train dataset to ModelArts Training Workspace
  if args.is_modelarts:# 当运行在ModelArts训练管理作业上时
      if not os.path.exists(args.train_data): # 在创建任务时指定的数据集路径，需要和config文件中配置一致
          os.makedirs(args.train_data)
      mox.file.copy_parallel(args.data_url, args.train_data) # 将obs桶内的数据集整个copy到ModelArts训练作业的工作目录下
  ```


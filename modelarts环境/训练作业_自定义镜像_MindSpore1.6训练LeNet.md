
## 训练作业_自定义MindSpore1.6镜像
1.  背景信息
    +   训练平台：成都人工智能计算中心-ModelArts服务
    +   底层硬件资源：atlas A800-9000 集群
    +   网络：lenet
    +   数据集：MNIST
    +   镜像：上传自定义MindSpore1.6镜像
    +   框架：MindSpore1.6 / CANN 5.0.4

2.  目的
    +   通过自定义镜像方式，基于lenet+MNIST的训练过程让使用者熟悉ModelArts服务中自定义镜像训练的功能
    自定义镜像官方使用文档[点击访问](https://support.huaweicloud.com/engineers-modelarts/modelarts_23_0241.html) 

## 准备工作
1.  代码准备
    选用仓库中 lenet 网络 - r1.6 [点击访问](https://gitee.com/mindspore/models/tree/r1.6/official/cv/lenet)，下载并存放于本地开发目录
   
2.  数据集准备  
    下载[MNIST](http://yann.lecun.com/exdb/mnist/)

##  训练前数据拷贝
1.  将代码和数据集上传到obs目录。

2.  将obs中代码和数据拷贝到modearts缓存路径中。
    +   修改代码中default_config.yaml配置文件
        ![输入图片说明](images/lenet_yaml_0509.png)  
    +   修改代码中train.py 
        ![输入图片说明](images/lenet_train_0509.png)
        ![输入图片说明](images/lenet_train_0509_1.png)
    细心的同学会发现代码只做了数据的拷贝modelarts中，代码怎么没有拷贝呢？
    因为自定义镜像的启动脚本run_train.sh中填入相关参数已经把代码拷贝进去了。
##  训练作业
1.  打开训练作业--启动自定义镜像方式--新版存在一点bug，进入旧版(网站后面输入trainingJobs)  
    ![输入图片说明](images/zuoye_0509_1.png)
    ![输入图片说明](images/zuoye_0509_2.png)
自定义镜像上传参考（https://gitee.com/Chengdu_Ascend/ascend-faq/blob/master/modelarts%E7%8E%AF%E5%A2%83/SWR%E5%AE%B9%E5%99%A8%E9%95%9C%E5%83%8F%E6%9C%8D%E5%8A%A1%E4%B8%8A%E4%BC%A0%E9%95%9C%E5%83%8F.md）  
 **运行命令如下，注意obs的路径改成s3** 
```
/bin/bash /home/work/run_train.sh 's3://bear/lenet/' 'lenet/train.py' '/tmp/log/train.log'
```
  
2.  创建后即可以在训练详情中看到日志及硬件使用情况  
        ![输入图片说明](images/lenet_log_0509_1.png)   
        注意，状态栏会显示当前的任务状态，一般显示如下：
        +   初始化：准备资源
        +   排队中：物理资源售罄，需等待其他任务结束释放资源，等待即可
        +   运行中: 正常训练中(由于实际运行的是代码，监控可能不会百分百准确，也要依靠日志的输出来查看实际的运行情况)
        +   运行成功：训练完成
        +   失败：由于异常训练任务终止(如果是自定义的代码，则需要依据代码的实际情况来判断)
3.  训练结束
        训练结果产物将输出在创建页面选择的训练输出位置
        ![输入图片说明](images/lenet_ckpt_0509.png)


**AICC Tools 是一款面向MindSpore开发者和华为人工智能计算中心，致力于帮助开发者快速将代码适配至计算中心，易化和规范化云上文件交互过程，简化云上日志显示，以此加速模型开发进度和降低云上集群调试难度。**   

aicc tools 中提供了 aicc_monitor装饰器接口，主要用于帮助开发者在AICC平台上监控训练算法的进程是否发生异常，在训练正常结束或者异常中断后，补获日志信息，同时将用户产生的最后一份文件回传保存至obs桶中。主要保存的内容包括：日志（plog、用户打印日志、错误日志、mindspore日志等）、模型权重文件、summary文件、其他用户保存的文件等。如果用户想要保存mindspore相关的图信息（graph、ir、dot、dump文件等）

原文链接 https://aicc-tools-docs.obs.cn-southwest-228.cdzs.cn/instruction/aicc_tools_docs/build/html/index.html

## 训练作业-AI引擎mindspore1.3
1.  背景信息
    +   训练平台：成都人工智能计算中心-ModelArts服务
    +   底层硬件资源：atlas A800-9000 集群
    +   网络：yolov4
    +   数据集：coco2017
    +   框架：MindSpore1.3 / CANN 5.0.2

2.  目的
    +   通过基于yolov4+coco2017的训练过程让使用者熟悉ModelArts服务中训练的功能

## 准备工作
1.  代码准备
    1.  选用仓库中 yolov4 网络 - r1.3 [点击访问](https://gitee.com/mindspore/models/tree/r1.3/official/cv/yolov4)，下载并存放于本地开发目录
    2.  yolov4预训练模型下载 - r1.3 [点击访问](https://www.mindspore.cn/resources/hub/details?MindSpore/ascend/1.3/yolov4_v1.3_coco2017)  
        

2.  数据集准备  
    下载[COCO2017](https://cocodataset.org/#download)保存于本地开发目录
    1.  [2017 Train images](http://images.cocodataset.org/zips/train2017.zip) [118K/18GB]
    2.  [2017 Val images](http://images.cocodataset.org/zips/val2017.zip) [5K/1GB]


##  训练
1.  修改default_config.yaml配置文件
    +   修改相关路径 
        ![输入图片说明](images/yolov4_yaml_0506.png)  
        将代码和预训练模型上传到obs目录，修改如下:
        ![输入图片说明](images/yolov4_yaml_0506_1.png)
        
    +   如果是自定义数据集，还需要修改如下字段：
        1.  per_batch_size
        2.  multi_scale，多尺度
        3.  anchor_scales，针对数据集使用聚类算法(kmeans)获取典型的搜索大小
        4.  num_classes,目标个数
        5.  out_channel，3 * (num_classes + 5)
        6.  labels，具体的标签
        
2.  上传代码及数据到OBS  
    直接上传到对应目录即可，注意不要将数据集放在代码目录中，不要忘记预训练模型的上传。

3.  创建训练
    1.  在HCSO中，进入 ModelArts-训练管理-训练作业-创建算法  
        ![输入图片说明](images/create_suanfa_0506.png)   
        ![输入图片说明](images/suanfa_0506.png)    
        ![输入图片说明](images/zuoye_0506_1.png)   
        如上填写信息，其余参数默认，创建即可。(若没有在yaml配置文件中填写相关配置，就需要在此处添加训练输入、训练输出、超参等)  
    2.  创建后即可以在训练详情中看到日志及硬件使用情况  
        ![输入图片说明](images/log_0506.png)   
        注意，状态栏会显示当前的任务状态，一般显示如下：
        +   初始化：准备资源
        +   排队中：物理资源售罄，需等待其他任务结束释放资源，等待即可
        +   运行中: 正常训练中(由于实际运行的是代码，监控可能不会百分百准确，也要依靠日志的输出来查看实际的运行情况)
        +   运行成功：训练完成
        +   失败：由于异常训练任务终止(如果是自定义的代码，则需要依据代码的实际情况来判断)
    3.  训练结束
        训练结果产物将输出在创建页面选择的训练输出位置
        ![输入图片说明](images/train_0506.png)


## 验证
1.  修改配置文件  
    修改default.yaml文件，将 `checkpoint_url` 路径改为 `OBS上训练产出的ckpt路径`；
    修改字段 `pretrained` 为 `"/cache/checkpoint_path/0-1_14658.ckpt"`，并上传修改后的配置文件到对应目录
    ![输入图片说明](images/yolov4_eval_yaml_0506.png)
    ![输入图片说明](images/yolov4_eval_yaml_0506_1.png)

3.  运行验证代码
    1.  在HCSO中，进入 ModelArts-训练管理-训练作业-创建  
        ![输入图片说明](images/yolov4_eval_0506.png)
        如上填写信息，创建训练即可。
        可以依据需求重新调整训练时诸如学习率、img_shape等参数达到需求效果即可。
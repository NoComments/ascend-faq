## 成都智算中心SWR容器镜像服务上传镜像
1.  背景信息
    +   训练平台：成都人工智能计算中心-ModelArts服务
    +   底层硬件资源：atlas A800-9000 集群
    +   容器镜像服务：上传自定义镜像  
2.  登录成都智算中心[点击访问](https://uconsole.cdzs.cn/#/mgt/modelarts)   
3.  点击容器镜像服务  
    ![输入图片说明](images/image_swr_0512.png)  
    ![输入图片说明](images/image_swr_0512_1.png)   
4.  创建组织完成后，上传镜像    
     **如需上传大于 2 GB的文件，请使用客户端上传，临时登录指令**   
    ![输入图片说明](images/image_swr_0512_2.png)   
    ![输入图片说明](../%E9%97%AE%E9%A2%98%E6%88%AA%E5%9B%BE/docker_0225_1.png)   
###  将本地服务器docker pull拉取的镜像上传SWR  
* 打开本地服务器，添加相关配置信息，即可临时登录指令   
    ![输入图片说明](images/image_swr_0512_3.png)  
* 添加成都智算中心IP到本地服务器hosts节点  
```
vim /etc/hosts  
添加
119.6.186.82     swr.cn-southwest-228.cdzs.cn
``` 
再添加docker证书信任  
```
vim /etc/docker/daemon.json
添加
"insecure-registries": ["swr.cn-southwest-228.cdzs.cn"]
重启docker
systemctl daemon-reload
systemctl restart docker
```
配置添加完成，即可登录成功   
![输入图片说明](images/image_swr_0512_5.png)

### 下载ascend_modelarts基础镜像   
1.  pytorch1.5版本[访问链接](https://ascendhub.huawei.com/#/detail/ascend-pytorch-arm-modelarts)  
    MindSpore1.7版本[访问链接](https://119.6.186.84:443/docker-images/mindspore1.7.tar?AWSAccessKeyId=LEL9XERA5ZJPNHHVWHS2&Expires=1683961802&Signature=r4e5XEsfpVJiQMc/Za3qKFL1DgA%3D)  
    下载最新cann版本镜像  
    ![输入图片说明](images/image_torch_0512.png)  
    ![输入图片说明](images/image_swr_0512_6.png)
2.镜像上传到SWR
    将下载的基础镜像tag然后push到智算中心SWR

```
docker login 智算中心临时登录指令
docker tag ascendhub.huawei.com/public-ascendhub/ascend-pytorch-arm-modelarts:21.0.3 swr.cn-southwest-228.cdzs.cn/xxx/pytorch1.5:21.0.3
docker push swr.cn-southwest-228.cdzs.cn/xxx/pytorch1.5:21.0.3
```
注：tag命令中 swr.cn-southwest-228.cdzs.cn/xxx/pytorch1.5:21.0.3 为刚才创建的{组织名称}/{镜像名称}:{版本名称}  
![输入图片说明](images/image_swr_0512_7.png)  

3.  现在即可在SWR中查看上传的自定义镜像
![输入图片说明](images/image_swr_0512_8.png)